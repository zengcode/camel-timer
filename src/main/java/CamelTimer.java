import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class CamelTimer {

    public static void main(String[] args) throws Exception {


        CamelContext context = new DefaultCamelContext();

        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("timer://test-timer?period=10s")
                        .bean(CustomBean.class);
            }
        });

        context.start();

        while (true) {}

    }
}
